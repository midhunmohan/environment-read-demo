package com.example.demo.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Created by midhun on 18/9/19.
 */
@ConfigurationProperties(prefix = "keytab")
public class KeyTabConfig {
    private String username;

    private String path;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
