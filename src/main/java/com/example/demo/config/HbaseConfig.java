package com.example.demo.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.sql.Connection;

/**
 * Created by midhun on 18/9/19.
 */
@Configuration
@EnableConfigurationProperties({KeyTabConfig.class})
public class HbaseConfig {

    @Bean
    public String getHbaseConnect(KeyTabConfig keyTabConfig) {
        System.out.println(keyTabConfig.getPath() + ":" + keyTabConfig.getUsername());
        return keyTabConfig.getPath();

    }
}
